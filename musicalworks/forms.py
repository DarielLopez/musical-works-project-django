from django import forms


class FormArticle(forms.Form):
    title = forms.CharField(label="Titulo")
    contributors = forms.CharField(label="Contributors")
    iswc = forms.CharField(label="ISWC")
