from django.shortcuts import render, HttpResponse, redirect
from musicalworks.models import MiappMusicalWorks  # para importar el modelo
from django.http import JsonResponse

# Create your views here.
# MVC= Modelo vista Controlador -> Acciones (metodos)
# MVT= Modelo Template Vista -> Acciones (metodos)

# de esta forma se hace el layout desde el archivo py


def articuloJson(request):
    # para devolver en Json y tiene que coincidir el nombre de la tabla que quieres ver
    articulo = list(MiappMusicalWorks.objects.values())
    return JsonResponse(articulo, safe=False)


def articulos(request):

    # esto sirve para filtrar

    articulos = MiappMusicalWorks.objects.filter(
        iswc__contains='') .filter(title__contains='')
    return render(request, 'create_article.html', {
        'articulos': articulos
    })


def borrar_articulo(request, id):
    articulo = MiappMusicalWorks.objects.get(pk=id)
    articulo.delete()

    return redirect('articulos')


def save_article(request):

    if request.method == 'GET':
        title = request.GET['title']
        contributors = request.GET['contributors']
        iswc = request.GET['iswc']

        articulo = MiappMusicalWorks(
            title=title,
            contributors=contributors,
            iswc=iswc,

        )
        articulo.save()

        return redirect('articulos')
    else:
        return HttpResponse(f"< h2 > No se ha podido crear el articulo < /h2 >")
