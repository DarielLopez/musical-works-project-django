from django.apps import AppConfig


class MiappConfig(AppConfig):
    name = 'musicalworks'
