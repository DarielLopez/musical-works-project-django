from django.db import models

# Create your models here.


class musical_works(models.Model):
    # se pudiera aplicar esta opcion para evitar los titulos duplicados (unique=True)
    title = models.CharField(max_length=150)
    contributors = models.CharField(max_length=150)
    iswc = models.CharField(max_length=150)  # unique=True
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'El titulo es %s los contribuidores son %s y el iswc es %s' % (self.title, self.contributors, self.iswc)


class MiappMusicalWorks(models.Model):
    title = models.CharField(max_length=150)
    contributors = models.CharField(max_length=150)
    iswc = models.CharField(max_length=150)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'El titulo es %s los contribuidores son %s y el iswc es %s' % (self.title, self.contributors, self.iswc)

    class Meta:
        managed = False
        db_table = 'miapp_musical_works'
