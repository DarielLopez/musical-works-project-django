import psycopg2
from psycopg2 import Error

connection = psycopg2.connect(host="localhost", user="postgres", password="123456", dbname="postgres")
cursor = connection.cursor()


create_table_query = '''CREATE TABLE musical_works
(title  TEXT NOT NULL ,
contributors  TEXT NOT NULL ,
iswc  TEXT NOT NULL );'''

cursor.execute(create_table_query)

f = open("works_metadata.csv", 'r')
cursor.copy_from(f, 'musical_works', sep=',')
connection.commit()
f.close()
